FROM python:3.9 
# Or any preferred Python version.
ADD *.py .
RUN pip install paho-mqtt hamradio 
CMD ["python", "./gateway.py"] 
# Or enter the name of your unique directory and parameter set.
