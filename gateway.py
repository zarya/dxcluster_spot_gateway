import ssl
import json
import base64
import paho.mqtt.client as mqtt
import configparser
from spot import Spot

def process_spot(client, data):
    global config
    line = data['PC'].split("^")
    spot = Spot(line[2])
    spot.setFreq(line[1])
    spot.setDateTime(line[3],line[4])
    spot.setComment(line[5])
    spot.setSpotter(line[6])
    spot.setOrigin(line[7])
    print(spot)
    client.publish('spotin/' + config['MQTT']['username'], spot.toJSON())


def on_connect(client, userdata, flags, rc, properties):
    print("Connected with result code "+str(rc))
    client.subscribe("pcin/#")

def on_message(client, userdata, msg):
    data = json.loads(msg.payload)
    if msg.topic.startswith("pcin"):
        decoded = base64.b64decode(data['PC']).decode('iso-8859-1')
        data['PC'] = decoded 
        if 'PCnr' in data and (data['PCnr'] == 61 or data['PCnr'] == 11):
            process_spot(client, data)

#Load configuration file
config = configparser.ConfigParser()
config.read('spotgateway.ini')

#Setup MQTT Client
client = mqtt.Client(protocol=mqtt.MQTTv5, transport='tcp')
client.on_connect = on_connect
client.on_message = on_message
client.tls_set(tls_version=ssl.PROTOCOL_TLS, cert_reqs=ssl.CERT_NONE)
client.tls_insecure_set(True)
client.username_pw_set(config['MQTT']['username'], password=config['MQTT']['password'])
client.connect(config['MQTT']['hostname'], port=int(config['MQTT']['port']), keepalive=60)

#Run MQTT Main loop
client.loop_forever()
