import json
import time
import datetime
from hamradio.bandplan import Bandplan, Band

class Spot:
    freq = 0
    band = ''
    spotcall = ''
    timestamp = 0
    comment = ''
    spotter = ''
    origin = ''
    ip = ''
    band = 'testband'

    bandplan = Bandplan()
    bandplan.add_band (Band (bandplan, '2.2km',   135.7e3,   137.8e3))
    bandplan.add_band (Band (bandplan, '630m',    472.0e3,   479.0e3))
    bandplan.add_band (Band (bandplan, '160m',   1810.0e3,  2000.0e3))
    bandplan.add_band (Band (bandplan, '80m',    3500.0e3,  3800.0e3))
    bandplan.add_band (Band (bandplan, '60m',    5351.3e3,  5366.5e3))
    bandplan.add_band (Band (bandplan, '40m',    7000.0e3,  7200.0e3))
    bandplan.add_band (Band (bandplan, '30m',   10100.0e3, 10150.0e3))
    bandplan.add_band (Band (bandplan, '20m',   14000.0e3, 14350.0e3))
    bandplan.add_band (Band (bandplan, '17m',   18068.0e3, 18168.0e3))
    bandplan.add_band (Band (bandplan, '15m',   21000.0e3, 21450.0e3))
    bandplan.add_band (Band (bandplan, '12m',   24890.0e3, 24990.0e3))
    bandplan.add_band (Band (bandplan, '10m',   28000.0e3, 29700.0e3))
    bandplan.add_band (Band (bandplan, '6m',       50.0e6,    52.0e6))
    bandplan.add_band (Band (bandplan, '2m',      144.0e6,   146.0e6))
    bandplan.add_band (Band (bandplan, '70cm',    430.0e6,   440.0e6))

    def __init__(self, spotcall):
        self.spotcall = spotcall
    def setFreq(self, freq):
        self.freq = float(freq)
        band = self.bandplan.lookup(float(freq + "e3"))
        if band:
            self.band = band.name
        else:
            print(freq)
            self.band = freq

    def setDateTime(self, date, t):
        dt = "%sT%s00-0000" % (date, t[:-1])
        ts = time.strptime(dt, "%d-%b-%YT%H%M%S%z")
        self.timestamp = int(time.mktime(ts))
    def setComment(self, msg):
        self.comment = msg.replace('"','')
    def setSpotter(self, call):
        self.spotter = call
    def setOrigin(self, origin):
        self.origin = origin
    def __str__(self):
        return("DX de %s: %s %s %s %s" % (self.spotter,self.freq,self.spotcall,self.comment,self.timestamp))
    def toJSON(self):
        data = {}
        data['freq'] = self.freq
        data['band'] = self.band
        data['spotcall'] = self.spotcall
        data['timestamp'] = self.timestamp
        data['comment'] = self.comment
        data['spotter'] = self.spotter
        data['origin'] = self.origin
        data['source'] = 'dxspider'
        return json.dumps(data)
